\c meta;

CREATE TABLE IF NOT EXISTS public.users (
  id SERIAL,
  address VARCHAR(42),
  chain VARCHAR(10),
  PRIMARY KEY (id)
);
