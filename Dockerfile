FROM node:lts-alpine3.16

WORKDIR /meta

COPY . /meta

RUN npm install
RUN ./node_modules/.bin/webpack ./public/js/handler.js --config webpack-config.js
RUN npx tsc

CMD ["node", "server.js"]