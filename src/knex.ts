import { knex } from 'knex'

const knexInstance = knex({
  client: 'postgres',
  connection: {
    host : 'kozinaki.net',
    port : 5432,
    user : 'postgres',
    password : 'postgres',
    database : 'meta'
  }   
})
  
interface User {
  id: number
  address: string
  chain: string
}
  
function getUserbyAddress(address: string) {
  return knexInstance<User>('users')
    .select()
    .where('address', address)
    .first()
    .then((user) => {
      if (user == null) {
        throw new Error("There is not user with address: " + address)
      }
      return user
    })
}
  
function saveUser(address: string, chainId: string) {
  return knexInstance<User>('users')
  .insert({address: address, chain: chainId})
  .returning('*')
  .catch(console.log)
}

export { getUserbyAddress, saveUser };