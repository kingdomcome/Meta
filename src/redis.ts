import * as redis from 'redis'

const SESSION_AGE = 3600 * 24

const redisClient = redis.createClient({
  url: 'redis://kozinaki.net',
  socket: {
    port: 6379
  }
})

redisClient.on('error', (err) => console.log('Redis Client Error', err));
redisClient.connect()

const setSession = async (key, value) => {
  await redisClient.SETEX(key, SESSION_AGE, value)
}

const getBySession = async (key) => {
  return redisClient.get(key)
}

export { setSession, getBySession }