const path = require('path')

module.exports = {
    devtool: 'source-map',
    mode: "development",
    entry: './handler.js',
    target: 'web',
    output: {
        path: path.resolve(__dirname, 'public/dist'),
        filename: 'bundle.js',
        libraryTarget: 'var',
        library: 'EntryPoint'
    },
    resolve: {
        extensions: ['.Webpack.js', '.web.js', '.ts', '.js', '.jsx', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.tsx$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'ts-loader'
                }
            }
        ]
    }
}