import { ethers } from 'ethers'

const auth = async() => {
  try {
    if (!window.ethereum) {
      Swal.fire({
        icon: 'error',
        title: 'The Metamask plugin is missing',
        text: 'No crypto wallet found. Please install it.'
      })
    }

    await ethereum.request({ method: 'eth_requestAccounts' })
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = provider.getSigner()
    const address = await signer.getAddress()
    const signature = await signer.signMessage(address)
    const chainId = await signer.getChainId()

    await fetch('http://kozinaki.net:8099/auth',
      { method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          address: address,
          signature: signature,
          chainId: chainId
        })
    })
    .then((response) => {
      return response.json()
    })
    .then((uuid) => {
      if (uuid != null) {
        hideAuth()
        showMe()
      }
    })
  } catch (err) {
    console.log(err.message)
  }
}

const me = async() => {
  try {
    await fetch('http://kozinaki.net:8099/me', { method: 'GET' })
    .then((response) => {
      return response.json()
    })
    .then((user) => {
      if (user != null) {
        document.getElementById('addressField').innerHTML = user.address;
        document.getElementById('chainField').innerHTML = user.chain;
        showInfo()
      } else {
        hideInfo()
      }
    })
  } catch(err) {
    console.log(err)
  }
}

try {
  fetch('http://kozinaki.net:8099/me', { method: 'GET' })
  .then((response) => {
    return response.json()
  })
  .then((user) => {
      if (user == null) {
        showAuth()
        hideMe()
      } else {
        showMe()
        hideAuth()
      }
  })
} catch(err) {
  console.log(err)
}


function showInfo() {
  changeVisibility("info", "block")
}

function hideInfo() {
  changeVisibility("auth", "none")
}

function showAuth() {
  changeVisibility("auth", "block")
}

function hideAuth() {
  changeVisibility("auth", "none")
}

function showMe() {
  changeVisibility("me", "block")
}

function hideMe() {
  changeVisibility("me", "none")
}

function changeVisibility(id, displaying) {
  const element = document.getElementById(id);
  element.style.display = displaying;
}

window.auth = auth;
window.me = me;