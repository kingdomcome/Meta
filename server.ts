import * as express from 'express' 
import { Express } from 'express'
import * as path from 'path'
import * as ethers from 'ethers'
import { v4 as uuidv4 } from 'uuid'
import * as cookieParser from 'cookie-parser'
import * as bodyParser from 'body-parser'
import { getUserbyAddress, saveUser } from './src/knex'
import { setSession, getBySession } from './src/redis'

const SESSION_ID = 'sessionId'
const COOKIE_AGE = 3600 * 24 * 1000
const SERVER_PORT = 8099

const app = express();
const staticPath = path.join(__dirname, './public')
app.use(express.static(staticPath))
app.use(cookieParser('key'))

const jsonParser = bodyParser.json()

app.post('/auth', jsonParser, async (req, res) => {
  console.log(req.body)
  const message = req.body.address
  const address = req.body.address
  const signature = req.body.signature
  const chainId = req.body.chainId
  const signerAddr = await ethers.utils.verifyMessage(message, signature)
  if (signerAddr !== address) {
    return res.json(null)
  }
  await getUserbyAddress(address)
  .catch((err) => {
    console.log(err.message)
    saveUser(address, chainId)
  })
  const uuid = uuidv4()
  setSession(uuid, address)
  res.cookie(SESSION_ID, uuid, {
      maxAge: COOKIE_AGE,
      httpOnly: true
  });
  setSession(uuid, address)
  res.json({ uuid: uuid })
})

app.get('/me', async (req, res) => {
  if (req.cookies.sessionId == null) {
    return res.json(null)
  }
  getBySession(req.cookies.sessionId)
  .then((address) => {
    if (address == null) {
      throw new Error("There is no session for user with: " + req.cookies.sessionId)
    }
    return getUserbyAddress(address)
  })
  .then((user) => {
    res.json(user)
  })
  .catch((err) => {
    console.log(err.message)
    res.json(null)
  })
})

app.listen(SERVER_PORT, () => {
    console.log(`⚡️[server]: Server is running at https://kozinaki.net:${SERVER_PORT}`)
})
